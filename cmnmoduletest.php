<?php

if (!defined('_PS_VERSION_'))
    exit;

class CmnModuleTest extends Module
{
    public function __construct()
    {
        $this->name = 'cmnmoduletest';
        $this->tab = 'back_office_features';
        $this->version = '1.0.0';
        $this->author = 'Comm.On';
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Test module');
        $this->description = $this->l('Test');
    }

    public function install()
    {
        return parent::install();
    }
}
